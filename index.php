<?php

require 'vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

use CityMap\Component\Request\Handler;

// Display all errors for development
error_reporting(E_ALL);

// 1. Get settings from config file
$settings = Yaml::parseFile('.configs/cm.yaml');

// 2. Request handler
$request_handler = Handler::getInstance($settings);

// 3. Get customer given an array of customer numbers
//$q = Yaml::parseFile('.parameters/cm.get_base_entry.yaml');

// Get customers using a geopolygon
//$q = Yaml::parseFile('.parameters/cm.get_business_listings_geo_polygon.yaml');

// 3. Get a list of customers for a given category
$q = Yaml::parseFile('.parameters/cm.get_business_listings.yaml');

// Request handler debugger
//dd($request_handler->post($q['method'], $q['parameters']));
$data = json_decode($request_handler->post($q['method'], $q['parameters']));
dd($data);
if($data != null){
    echo '
    <html>
    <head>
    ';
    if(!empty($data->stylesheets)){
        foreach ($data->stylesheets as $css) {
            echo '<link rel="stylesheet" href="'.$css[0].'">';
        }
    }
    echo'
    </head>
    <body>
    '.utf8_decode($data->display).'
    </body>
    </html>';
}

?>
