# CityMap / Using the API

This is a full working example of how to use the CityMap API.

You will need at lease the version 7 of PHP to be able to use this code.

First run `composer install` to install all necessary packages.

> We use the Symfony components [YAML](https://symfony.com/components/Yaml) and [VarDumper](https://symfony.com/components/VarDumper).
 Please refer to their documentation if you don't know how to use them.

For using the city-map API, we need 3 things:
  1. A yaml configuration file containing the following parameters:
   - **response_type**: html, json
   - **host**: https://my.cmpowersite.com/api/
   - **public_key**: CustomerPublicKey
   - **private_key**: CustomerPrivateKey

  2. A yaml file containing the parameters for the API endpoint which you want to use.

  3. The city-map request handler which makes the calls to the API.


This is a set of examples for getting:
  1. Customer data using an array of customer numbers
  2. Business listings using geopolygon coordinates and a business category
  3. Business listings using a location as center point, a radius and a business category


> In order to get both API keys for a customer, please contact [pg@flexbone.systems](mailto:pg@flexbone.systems)

Just take a look at the code and feel **free to ask any questions!**
